﻿namespace QUANLYSIEUTHI
{
    partial class f_GianHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.msds = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnluu = new System.Windows.Forms.Button();
            this.btnxoa = new System.Windows.Forms.Button();
            this.btnsua = new System.Windows.Forms.Button();
            this.btnthem = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtbmanv = new System.Windows.Forms.TextBox();
            this.btnmanv = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtbvitri = new System.Windows.Forms.TextBox();
            this.btnvitri = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.txtbtengh = new System.Windows.Forms.TextBox();
            this.btntengh = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtbmagh = new System.Windows.Forms.TextBox();
            this.btnmagh = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.msds)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1175, 712);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(514, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 36);
            this.label1.TabIndex = 3;
            this.label1.Text = "Gian Hàng";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.msds);
            this.panel4.Location = new System.Drawing.Point(555, 79);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(617, 627);
            this.panel4.TabIndex = 2;
            // 
            // msds
            // 
            this.msds.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.msds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.msds.Location = new System.Drawing.Point(25, 0);
            this.msds.Name = "msds";
            this.msds.RowHeadersWidth = 62;
            this.msds.RowTemplate.Height = 28;
            this.msds.Size = new System.Drawing.Size(595, 595);
            this.msds.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnluu);
            this.panel3.Controls.Add(this.btnxoa);
            this.panel3.Controls.Add(this.btnsua);
            this.panel3.Controls.Add(this.btnthem);
            this.panel3.Location = new System.Drawing.Point(19, 513);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(516, 177);
            this.panel3.TabIndex = 1;
            // 
            // btnluu
            // 
            this.btnluu.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnluu.Location = new System.Drawing.Point(298, 116);
            this.btnluu.Name = "btnluu";
            this.btnluu.Size = new System.Drawing.Size(134, 45);
            this.btnluu.TabIndex = 4;
            this.btnluu.Text = "Lưu";
            this.btnluu.UseVisualStyleBackColor = true;
            this.btnluu.Click += new System.EventHandler(this.btnluu_Click);
            // 
            // btnxoa
            // 
            this.btnxoa.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnxoa.Location = new System.Drawing.Point(60, 116);
            this.btnxoa.Name = "btnxoa";
            this.btnxoa.Size = new System.Drawing.Size(126, 45);
            this.btnxoa.TabIndex = 3;
            this.btnxoa.Text = "Xóa";
            this.btnxoa.UseVisualStyleBackColor = true;
            this.btnxoa.Click += new System.EventHandler(this.btnxoa_Click);
            // 
            // btnsua
            // 
            this.btnsua.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnsua.Location = new System.Drawing.Point(298, 38);
            this.btnsua.Name = "btnsua";
            this.btnsua.Size = new System.Drawing.Size(134, 45);
            this.btnsua.TabIndex = 2;
            this.btnsua.Text = "Sửa";
            this.btnsua.UseVisualStyleBackColor = true;
            this.btnsua.Click += new System.EventHandler(this.btnsua_Click);
            // 
            // btnthem
            // 
            this.btnthem.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnthem.Location = new System.Drawing.Point(60, 38);
            this.btnthem.Name = "btnthem";
            this.btnthem.Size = new System.Drawing.Size(126, 45);
            this.btnthem.TabIndex = 1;
            this.btnthem.Text = "Thêm";
            this.btnthem.UseVisualStyleBackColor = true;
            this.btnthem.Click += new System.EventHandler(this.btnthem_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Location = new System.Drawing.Point(3, 79);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(546, 627);
            this.panel2.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.txtbmanv);
            this.panel8.Controls.Add(this.btnmanv);
            this.panel8.Location = new System.Drawing.Point(16, 339);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(516, 89);
            this.panel8.TabIndex = 3;
            // 
            // txtbmanv
            // 
            this.txtbmanv.Location = new System.Drawing.Point(240, 38);
            this.txtbmanv.Name = "txtbmanv";
            this.txtbmanv.Size = new System.Drawing.Size(248, 26);
            this.txtbmanv.TabIndex = 1;
            // 
            // btnmanv
            // 
            this.btnmanv.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnmanv.Location = new System.Drawing.Point(18, 27);
            this.btnmanv.Name = "btnmanv";
            this.btnmanv.Size = new System.Drawing.Size(186, 45);
            this.btnmanv.TabIndex = 0;
            this.btnmanv.Text = "Mã Nhân Viên";
            this.btnmanv.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.txtbvitri);
            this.panel7.Controls.Add(this.btnvitri);
            this.panel7.Location = new System.Drawing.Point(16, 231);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(516, 81);
            this.panel7.TabIndex = 2;
            // 
            // txtbvitri
            // 
            this.txtbvitri.Location = new System.Drawing.Point(240, 28);
            this.txtbvitri.Name = "txtbvitri";
            this.txtbvitri.Size = new System.Drawing.Size(248, 26);
            this.txtbvitri.TabIndex = 1;
            // 
            // btnvitri
            // 
            this.btnvitri.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnvitri.Location = new System.Drawing.Point(18, 17);
            this.btnvitri.Name = "btnvitri";
            this.btnvitri.Size = new System.Drawing.Size(186, 45);
            this.btnvitri.TabIndex = 0;
            this.btnvitri.Text = "Vị Trí";
            this.btnvitri.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.txtbtengh);
            this.panel6.Controls.Add(this.btntengh);
            this.panel6.Location = new System.Drawing.Point(16, 113);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(516, 93);
            this.panel6.TabIndex = 1;
            // 
            // txtbtengh
            // 
            this.txtbtengh.Location = new System.Drawing.Point(240, 24);
            this.txtbtengh.Name = "txtbtengh";
            this.txtbtengh.Size = new System.Drawing.Size(248, 26);
            this.txtbtengh.TabIndex = 1;
            // 
            // btntengh
            // 
            this.btntengh.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btntengh.Location = new System.Drawing.Point(18, 24);
            this.btntengh.Name = "btntengh";
            this.btntengh.Size = new System.Drawing.Size(186, 45);
            this.btntengh.TabIndex = 0;
            this.btntengh.Text = "Tên Gian Hàng";
            this.btntengh.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.txtbmagh);
            this.panel5.Controls.Add(this.btnmagh);
            this.panel5.Location = new System.Drawing.Point(16, 15);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(516, 76);
            this.panel5.TabIndex = 0;
            // 
            // txtbmagh
            // 
            this.txtbmagh.Location = new System.Drawing.Point(240, 29);
            this.txtbmagh.Name = "txtbmagh";
            this.txtbmagh.Size = new System.Drawing.Size(248, 26);
            this.txtbmagh.TabIndex = 1;
            // 
            // btnmagh
            // 
            this.btnmagh.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnmagh.Location = new System.Drawing.Point(18, 17);
            this.btnmagh.Name = "btnmagh";
            this.btnmagh.Size = new System.Drawing.Size(186, 47);
            this.btnmagh.TabIndex = 0;
            this.btnmagh.Text = "Mã Gian Hàng";
            this.btnmagh.UseVisualStyleBackColor = true;
            // 
            // f_GianHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1199, 736);
            this.Controls.Add(this.panel1);
            this.Name = "f_GianHang";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gian Hàng";
            this.Load += new System.EventHandler(this.f_GianHang_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.msds)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView msds;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnxoa;
        private System.Windows.Forms.Button btnsua;
        private System.Windows.Forms.Button btnthem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox txtbmanv;
        private System.Windows.Forms.Button btnmanv;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txtbvitri;
        private System.Windows.Forms.Button btnvitri;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox txtbtengh;
        private System.Windows.Forms.Button btntengh;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtbmagh;
        private System.Windows.Forms.Button btnmagh;
        private System.Windows.Forms.Button btnluu;
    }
}